const parse = require('./table-parser.js')

describe('table-parser', () => {
  describe(`Parsing simple ascii`, () => {
    it(`parses a table with no columns and no rows`, () => {
      const table = ``
      expect(parse(table)).toEqual({
        header: [],
        rows: [],
      })
    })
    describe(`Parses a table with 1 columns and 0 rows`, () => {
      it('Case 1', () => {
        const table = `| id |`
        expect(parse(table)).toEqual({
          header: ['id'],
          rows: [],
        })
      })
      it('Case 2', () => {
        const table = `| longerHeader |`
        expect(parse(table)).toEqual({
          header: ['longerHeader'],
          rows: [],
        })
      })
      it('Case 3', () => {
        const table = `|  id  |`
        expect(parse(table)).toEqual({
          header: ['id'],
          rows: [],
        })
      })
    })
    it(`Parses a table with 1 columns and 1 rows`, () => {
      const table = `| id |
| 1 |`
      expect(parse(table)).toEqual({
        header: ['id'],
        rows: [
          {
            id: '1',
          },
        ],
      })
    })
    it(`Parses a table with 1 columns and 2 rows`, () => {
      const table = `| id |
| 1 |
| 2 |`
      expect(parse(table)).toEqual({
        header: ['id'],
        rows: [
          {
            id: '1',
          },
          {
            id: '2',
          },
        ],
      })
    })
    it(`Parses a table with 3 columns and 0 rows`, () => {
      const table = `| id | name | description |`
      expect(parse(table)).toEqual({
        header: ['id', 'name', 'description'],
        rows: [],
      })
    })
    it(`Parses a table with 3 columns and 3 rows`, () => {
      const table = `| id | name | description |
| 1 | test1 | description1 |
| 2 | test2 | description2 |`
      expect(parse(table)).toEqual({
        header: ['id', 'name', 'description'],
        rows: [
          {
            id: '1',
            name: 'test1',
            description: 'description1',
          },
          {
            id: '2',
            name: 'test2',
            description: 'description2',
          },
        ],
      })
    })
  })
})
