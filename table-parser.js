const findCellValues = (rowString) =>
  rowString
    .split('|')
    .map((word) => word.trim())
    .filter((word) => word !== '')

const zipObj = (keys, values) => {
  let obj = {}
  keys.forEach((key, index) => {
    obj[key] = values[index]
  })
  return obj
}

const parse = (table) => {
  const [headerString, ...rowStrings] = table.split('\n')
  const header = findCellValues(headerString)
  const rows = rowStrings
    .map((rowString) => rowString.trim())
    .filter((rowString) => rowString !== '' && rowString[0] !== '#')
    .map((rowString) => zipObj(header, findCellValues(rowString)))
  return { header, rows }
}

module.exports = parse
